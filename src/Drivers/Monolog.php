<?php
namespace Soen\Log\Drivers;

use Monolog\Logger;

class Monolog
{
    public function __construct()
    {
        return new Logger();
    }
}