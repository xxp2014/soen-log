<?php


namespace Soen\Log;


use DateTimeZone;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\ProcessHandler;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Handler\StreamHandler;
use Soen\Log\Handler\ConsoleHandler;
use Soen\Log\Handler\RotatingFileResetHandler;

class Logger extends \Monolog\Logger
{
    public $logDriver;
    public $name = 'app_log';
    public $dateFormat = 'Y-m-d H:i:s';
    public $logFile;
    public $formatter;
    public $logger;
    public $level = 'info';
    public $handlers = [];
    function __construct()
    {
        $allLevel = [
            'info'      =>  \Monolog\Logger::INFO,
            'debug'     =>  \Monolog\Logger::DEBUG,
            'notice'    =>  \Monolog\Logger::NOTICE,
            'warning'   =>  \Monolog\Logger::WARNING,
            'error'     =>  \Monolog\Logger::ERROR,
        ];
        $this->level = strtolower($this->level);
        if(!isset($allLevel[$this->level])){
            throw new \RuntimeException("log组件配置level异常，不存在 {$this->level} 这个日志级别");
        }
        $this->level = $allLevel[$this->level];
        $this->logger = $this->createLogger();
    }

//    function createLogFormat(){
//        $output = "%datetime% | %level_name% | %message% | %context% | %extra%\n";
//        $this->formatter = new LineFormatter($output, $this->dateFormat);
//        return $this->formatter;
//    }

    /**
     * 创建一个 monolog
     * @return \Monolog\Logger
     */
    public function createLogger(){
//        $stream  = new StreamHandler($this->logFile, $this->level);
//        $stream->setFormatter($this->createLogFormat());
//        $handle = new RotatingFileResetHandler($this->logFile, 2,  $this->level);
//        $handle = new ConsoleHandler($this->level);
//        $handle->setFormatter($this->createLogFormat());
//        $handle = new ProcessHandler($this->logFile, $this->level);
        $handlers = [];
        foreach ($this->handlers as $handler){
            $handlers[] = (new \ReflectionClass($handler['class']))->newInstanceArgs($handler['args']);
        }
        $logger = new \Monolog\Logger($this->name, $handlers);
        return $logger;
    }

    /**
     * Adds a log record at an arbitrary level.
     *
     * This method allows for compatibility with common interfaces.
     *
     * @param mixed  $level   The log level
     * @param string $message The log message
     * @param array  $context The log context
     */
    public function log($level, $message, array $context = []): void
    {
        $this->logger->log($level, $message, $context);
    }

    /**
     * Adds a log record at the DEBUG level.
     *
     * This method allows for compatibility with common interfaces.
     *
     * @param string $message The log message
     * @param array  $context The log context
     */
    public function debug($message, array $context = []): void
    {
        $this->logger->debug($message, $context);
    }

    /**
     * Adds a log record at the INFO level.
     *
     * This method allows for compatibility with common interfaces.
     *
     * @param string $message The log message
     * @param array  $context The log context
     */
    public function info($message, array $context = []): void
    {
        $this->logger->info($message, $context);
    }

    /**
     * Adds a log record at the NOTICE level.
     *
     * This method allows for compatibility with common interfaces.
     *
     * @param string $message The log message
     * @param array  $context The log context
     */
    public function notice($message, array $context = []): void
    {
        $this->logger->notice($message, $context);
    }

    /**
     * Adds a log record at the WARNING level.
     *
     * This method allows for compatibility with common interfaces.
     *
     * @param string $message The log message
     * @param array  $context The log context
     */
    public function warning($message, array $context = []): void
    {
        $this->logger->warning($message, $context);
    }

    /**
     * Adds a log record at the ERROR level.
     *
     * This method allows for compatibility with common interfaces.
     *
     * @param string $message The log message
     * @param array  $context The log context
     */
    public function error($message, array $context = []): void
    {
        $this->logger->error($message, $context);
    }

    /**
     * Adds a log record at the CRITICAL level.
     *
     * This method allows for compatibility with common interfaces.
     *
     * @param string $message The log message
     * @param array  $context The log context
     */
    public function critical($message, array $context = []): void
    {
        $this->logger->critical($message, $context);
    }

    /**
     * Adds a log record at the ALERT level.
     *
     * This method allows for compatibility with common interfaces.
     *
     * @param string $message The log message
     * @param array  $context The log context
     */
    public function alert($message, array $context = []): void
    {
        $this->logger->alert($message, $context);
    }

    /**
     * Adds a log record at the EMERGENCY level.
     *
     * This method allows for compatibility with common interfaces.
     *
     * @param string $message The log message
     * @param array  $context The log context
     */
    public function emergency($message, array $context = []): void
    {
        $this->logger->emergency($message, $context);
    }





}